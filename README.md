# CHTHULUCENE [VR i3 2020]

unity 3d resources (scripts, textures, scenes, etc.) for the VR module organised by [l.rogliano](https://www.lea-rogliano.com) & [frankiezafe](http://frankiezafe.org) at [interface3](https://www.interface3.be/), gaming programme

the module is based on [Donnna Haraway](https://people.ucsc.edu/~haraway/)'s book: [Staying with the Trouble, Making Kin in the Chthulucene](https://www.dukeupress.edu/staying-with-the-trouble)

[general presentation](https://www.lea-rogliano.com/blog/2020/8/18/le-18-aot-cette-anne-latelier-vr-que-je-donne-avec-franois-zajga-chez-interface-3-aura-pour-thmatique-le-chthulucene-de-dona-jharaway)

## content

the code available in wire & metaballs is not production ready: they continously compute meshes without optimisation

### wire

`path: unity/Assets/pack/wire`

contains a script that generate geometries along a list of 3d positions

editor allows you to manually insert 3d positions, modify the perimeter's resolution and the amount of segment subdivision

the generate mesh contains 2 uv maps, set to `uv1` and `uv2`:

- *per segment uv*: each segment have a V axis from 0 to 1
- *relative to length*: V are generated depending on the distance to origin (the fist point in the list of 3d positions)

![unity 3d scene screenshoyt](https://polymorph.cool/wp-content/uploads/2020/08/wire_scene.jpg)

### metaballs

`path: unity/Assets/pack/metaballs`

adaptation of (Nesh108)[https://github.com/Nesh108]'s [Unity MetaBalls Liquids](https://github.com/Nesh108/Unity_MetaBalls_Liquids) project

the class [OriginalMCBlob](https://gitlab.com/polymorphcool/i3_2020/-/blob/master/unity/Assets/pack/metaballs/scripts/OriginalMCBlob.cs) is now able to:

- track position of multiple GameObjects, that can be animated or physicsed;
- attract the GameObjects that have RigidBody component towards the barycenter of the metaball;

a component [OriginalMCBlobSource](https://gitlab.com/polymorphcool/i3_2020/-/blob/master/unity/Assets/pack/metaballs/scripts/OriginalMCBlobSource.cs) can be attached to GameObjects interacting with the blob to control the weight of the object

*demo with gravity*

![screenshot of the metaball attached to balls falling on the floor](https://polymorph.cool/wp-content/uploads/2020/08/fluid_gravity01.jpg)

*demo without gravity*

![screenshot of the metaball attached to balls floating freely](https://polymorph.cool/wp-content/uploads/2020/08/fluid_space.jpg)

made in Unity 2018.2.4