﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ViveHandFollower : MonoBehaviour
{
    public SteamVR_Action_Boolean trig_action;
    public SteamVR_Action_Vector3 pos_action;
    public SteamVR_Input_Sources handType;

    //public Valve.VR.InteractionSystem.Player player = null;
    //private List<Valve.VR.InteractionSystem.Hand> tracked_hands = null;

    // Start is called before the first frame update
    void Start()
    {

        trig_action.AddOnStateDownListener(TriggerDown, handType);
        trig_action.AddOnStateUpListener(TriggerUp, handType);
        
    }

    public void TriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Debug.Log("Trigger is up");
    }
    public void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Debug.Log("Trigger is down");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
