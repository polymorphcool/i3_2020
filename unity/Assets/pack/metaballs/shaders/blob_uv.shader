﻿Shader "i3/blob_uv"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				float3 norm_v = normalize(v.vertex.xyz);
				// from https://en.wikipedia.org/wiki/UV_mapping#Finding_UV_on_a_sphere
				//float2 new_uv = float2(
				//	atan(norm_v.z / norm_v.x) / (2.0 * 3.1415926535898),
				//	asin(norm_v.y) / 3.1415926535898);
				// from https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
				//float r = 1;
				//float phi = atan( norm_v.y / norm_v.x );
				//float theta = acos(norm_v.z);
				//float2 new_uv = float2(phi / 3.1415926535898, theta / 3.1415926535898);
				// custom, using UP and FRONT dot products
				float2 new_uv =
					norm_v.xy * dot(norm_v, float3(0.0, 0.0, 1.0)) +
					norm_v.xz * dot(norm_v, float3(0.0, 1.0, 0.0)) +
					norm_v.yz * dot(norm_v, float3(1.0, 0.0, 0.0))
				;
                o.uv = TRANSFORM_TEX(new_uv, _MainTex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
