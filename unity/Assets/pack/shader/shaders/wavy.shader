﻿Shader "i3/wavy"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(-5,5)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Height("Height", Range(-100,100)) = 1.0
		_Speed("Speed", Range(0,100)) = 1.0
		_MultX("MultX", Range(-100,100)) = 1.0
		_MultY("MultY", Range(-100,100)) = 1.0
		_MultZ("MultZ", Range(-100,100)) = 1.0
		_OffsetX("OffsetX", Range(-3,3)) = 0.0
		_OffsetY("OffsetY", Range(-3,3)) = 0.0
		_OffsetZ("OffsetZ", Range(-3,3)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Geometry" }
        LOD 200

		//Back regular pass
		//ZWrite true
		//Cull back
		//ZTest true

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
		// yoooo
        #pragma target 3.0

        sampler2D _MainTex;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Height;
		float _Speed;
		float _MultX;
		float _MultY;
		float _MultZ;
		float _OffsetX;
		float _OffsetY;
		float _OffsetZ;

        struct Input
        {
            float2 uv_MainTex;
        };

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float mx = (1.0 + sin((_Time[1] * _Speed + v.vertex.x + _OffsetX) * _MultX)) * 0.5;
			float my = (1.0 + sin((_Time[1] * _Speed + v.vertex.y + _OffsetY) * _MultY)) * 0.5;
			float mz = (1.0 + sin((_Time[1] * _Speed + v.vertex.z + _OffsetZ) * _MultZ)) * 0.5;
			v.vertex.xyz += v.normal * (mx + my +mz) / 3.0 * _Height;
		}

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
		// put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            
			// Albedo comes from a texture tinted by color
			fixed4 albedo_color = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = albedo_color.xyz;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = albedo_color.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
