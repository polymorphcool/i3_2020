﻿Shader "i3/albedo_mixed"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _Albedo0Tex ("Albedo0 (RGB)", 2D) = "white" {}
		_Albedo1Tex("Albedo1 (RGB)", 2D) = "white" {}
		_MixTex("Mix (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(-5,5)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Geometry" }
        LOD 200

		//Back regular pass
		//ZWrite true
		//Cull back
		//ZTest true

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
		// yoooo
        #pragma target 3.0

        sampler2D _Albedo0Tex;
		sampler2D _Albedo1Tex;
		sampler2D _MixTex;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

        struct Input
        {
            float2 uv_Albedo0Tex;
			float2 uv_Albedo1Tex;
			float2 uv_MixTex;
        };

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
		}

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			// Albedo comes from a texture tinted by color
            fixed4 c_alb0 = tex2D(_Albedo0Tex, IN.uv_Albedo0Tex) * _Color;
			fixed4 c_alb1 = tex2D(_Albedo1Tex, IN.uv_Albedo1Tex) * _Color;
			fixed4 c_mix = tex2D(_MixTex, IN.uv_MixTex) * _Color;
            o.Albedo = c_alb0.rgb * c_mix.r + c_alb1.rgb * (1 - c_mix.r);
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c_alb0.a * c_mix.r + c_alb1.a * (1 - c_mix.r);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
