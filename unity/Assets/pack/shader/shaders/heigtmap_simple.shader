﻿Shader "i3/heightmap_simple"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DisplacementTex("Displacement (RGB)", 2D) = "white" {}
		_NormalTex("Normal (NORMAL)", 2D) = "bump" {}
        _Glossiness ("Smoothness", Range(-5,5)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Height("Height", Range(-100,100)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Geometry" }
        LOD 200

		//Back regular pass
		//ZWrite true
		//Cull back
		//ZTest true

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
		// yoooo
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _DisplacementTex;
		float4 _DisplacementTex_ST;
		sampler2D _NormalTex;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Height;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_NormalTex;
        };

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float2 displacement_uv = v.texcoord.xy * _DisplacementTex_ST.xy + _DisplacementTex_ST.zw;
			fixed4 color = tex2Dlod(_DisplacementTex, float4(displacement_uv,0,0));
			v.vertex.xyz += v.normal * color.r * _Height;
		}

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            
			// Albedo comes from a texture tinted by color
			fixed4 albedo_color = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = albedo_color.xyz;
			o.Normal = UnpackScaleNormal(tex2D(_NormalTex, IN.uv_NormalTex), 1.0);
			//o.Albedo = o.Normal;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = albedo_color.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
