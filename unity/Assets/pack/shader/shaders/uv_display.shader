﻿Shader "i3/uv_display" {
	Properties
	{
		_uvx("uvx", Range(0,3)) = 0
	}
	SubShader{
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			int _uvx;

			// vertex input: position, UV
			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 texcoord3 : TEXCOORD4;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			v2f vert(appdata v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				if (_uvx == 1) {
					o.uv = float4(v.texcoord1.xy, 0, 0);
				} else if (_uvx == 2) {
					o.uv = float4(v.texcoord2.xy, 0, 0);
				} else if (_uvx == 3) {
					o.uv = float4(v.texcoord3.xy, 0, 0);
				} else {
					o.uv = float4(v.texcoord0.xy, 0, 0);
				}
				return o;
			}

			half4 frag(v2f i) : SV_Target {
				half4 c = frac(i.uv);
				if (any(saturate(i.uv) - i.uv))
					c.b = 0.5;
				return c;
			}
			ENDCG
		}
	}
}